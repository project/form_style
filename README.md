# Form Style

This module gives an overview of all form elements. It is helpful to test the
usability and accessibility of forms.

Among other usage this module was, and is, instrumental in the development of
Inline Form Errors, Claro and Olivero.

**Note** that anyone with the 'access content' permission can use this module.
And it should therefore not be enabled in production sites.

## Installation

Install and enable this module as any other Drupal module, you can follow the
Drupal [documentation](
https://www.drupal.org/docs/8/extending-drupal-8/installing-modules).

## Usage

The page with all form elements can be visited at `/admin/appearance/form_style`.

Form settings can be found at `admin/config/form_style`.
